@extends('layouts.admin')

@section('title', 'Start page')

@section('content')
    <div class="card">
        <div class="card-header">Info</div>

        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item">
                    {{  link_to_route('admin.posts.index', 'Posts') }}
                    <span class="badge badge-pill badge-secondary">{{ $posts }}</span>
                </li>
                <li class="list-group-item">
                    {{  link_to_route('admin.categories.index', 'Categories') }}
                    <span class="badge badge-pill badge-secondary">{{ $categories }}</span>
                </li>
            </ul>
        </div>
    </div>
@endsection
