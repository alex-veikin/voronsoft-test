@extends('layouts.site')

@section('title', ('Post - ' . ($post->title ?? 'Untitled')))

@section('content')
    <div class="card">
        <div class="card-header">
            {{ link_to(url()->previous(), '<', ['class' => 'btn btn-secondary btn-sm mr-2']) }}
            Post - {{ $post->title ?? 'Untitled' }}
        </div>

        <div class="card-body">
            <p class="font-italic small">{{ $post->created_at }}</p>

            <p>
                @forelse($post->categories as $category)
                    {{ link_to_route('categories.show', ($category->title ?? 'Untitled'), ['category' => $category]) }}
                @empty
                    No categories
                @endforelse
            </p>

            <p>{{ $post->content }}</p>
        </div>
    </div>
@endsection
