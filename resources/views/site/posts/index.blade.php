@extends('layouts.site')

@section('title',
'Posts'
 . ($posts->lastPage() > 1 ? (' - page ' . $posts->currentPage()) : '')
 )

@section('content')
    <div class="card">
        <div class="card-header">
            Posts

            @if($posts->lastPage() > 1)
                {{ ': ' . $posts->firstItem() . ' - ' . $posts->lastItem() }}
            @endif
        </div>

        <div class="card-body">
            <ul class="list-group mb-4">
                @forelse($posts as $post)
                    <li class="list-group-item">
                        <span class="mr-2">{{ $posts->firstItem() + $loop->index }}</span>
                        {{  link_to_route('posts.show', ($post->title ?? 'Untitled'), ['post' => $post]) }}
                    </li>
                @empty
                    <li class="list-group-item">No posts</li>
                @endforelse
            </ul>

            {{ $posts->links() }}
        </div>
    </div>
@endsection
