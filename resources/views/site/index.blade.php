@extends('layouts.site')

@section('title', 'Start page')

@section('content')
    <div class="card">
        <div class="card-header">Last {{ count($posts) }} posts</div>

        <div class="card-body">
            <ul class="list-group">
                @foreach($posts as $post)
                    <li class="list-group-item">
                        <span class="mr-2">{{ $loop->iteration }}</span>
                        {{  link_to_route('posts.show', $post->title, ['post' => $post], []) }}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
