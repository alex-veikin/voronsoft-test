@extends('layouts.site')

@section('title', 'Categories')

@section('content')
    <div class="card">
        <div class="card-header">All categories</div>

        <div class="card-body">
            <ul class="list-group">
                @foreach($categories as $category)
                    <li class="list-group-item">
                        <span class="mr-2">{{ $loop->iteration }}</span>

                        {{  link_to_route('categories.show', ($category->title ?? 'Untitled'), ['category' => $category]) }}

                        <span class="badge badge-pill badge-secondary">{{ $category->posts_count }}</span>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
