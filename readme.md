## Инфо

- Laravel 5.7
- Vue.js


## Установка

- склонировать проект: `git clone https://alex-veikin@bitbucket.org/alex-veikin/voronsoft-test.git`
- перейти в каталог: `cd voronsoft-test`
- копировать файл окружения: `cp .env.example .env`
- отредактировать данные для подключения к базе файле .env
 (по-умолчанию: база - **voronsoft-test**, пользователь - **homestead**, пароль - **secret**)
- установить зависимости: `composer install`
- сгенерировать ключ приложения: `php artisan key:generate`
- закешировать настройки: `php artisan config:cache`
- заполнить базу данных тестовыми данными: `php artisan migrate --seed`

После выполнения в базе будут созданы:

- 12 категорий,
- 40 публикаций с привязками к категориям,
- пользователь: имя - **Admin**, email - **voronsoft@test.com**, пароль - **123456**

Вход в панель управления: `http://домен/admin`


## Компиляция (опционально)

Если нужно скомпилировать vue компоненты, то потребуется:

- установить npm зависимости: `npm install`
- скомпилировать: `npm run dev` (`npm run prod`)
- компиляция при изменении файлов: `npm run watch` (browsersync)

