<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Получаем массив с id всех категорий
        $category_list = \App\Category::pluck('id')->toArray();

        // Создаем 40 публикаций
        factory(App\Post::class, 40)
            ->create()
            // Перебираем созданные публикации
            ->each(function (\App\Post $post) use ($category_list) {
                // Выбираем от 1 до 3 случайных категорий
                $categories = array_random($category_list, random_int(1, 3));

                // Привязываем выбранные категории к публикации
                $post->categories()->sync($categories);
            });
    }
}
