<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'title' => 'HTML',
            ],
            [
                'title' => 'CSS',
            ],
            [
                'title' => 'SASS',
            ],
            [
                'title' => 'JavaScript',
            ],
            [
                'title' => 'jQuery',
            ],
            [
                'title' => 'Vue.js',
            ],
            [
                'title' => 'PHP',
            ],
            [
                'title' => 'Java',
            ],
            [
                'title' => 'SQL',
            ],
            [
                'title' => 'Laravel',
            ],
            [
                'title' => 'Symfony',
            ],
            [
                'title' => 'Yii',
            ],
        ]);
    }
}
