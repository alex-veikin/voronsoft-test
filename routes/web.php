<?php

// Сайт
Route::get('/', 'IndexController@index')->name('start');
Route::resource('posts', 'PostController')
     ->only(['index', 'show']);
Route::resource('categories', 'CategoryController')
     ->only(['index', 'show']);

// Админпанель
Route::group([
    'prefix'     => 'admin',
    'middleware' => 'auth',
    'namespace'  => 'Admin',
    'as'         => 'admin.',
], function () {
    Route::get('/', 'IndexController@index')->name('index');

    Route::get('posts', 'IndexController@posts')->name('posts.index');
    Route::get('posts/fetch', 'PostController@index');
    Route::resource('posts', 'PostController')
         ->only(['store', 'update', 'destroy']);

    Route::get('categories', 'IndexController@categories')->name('categories.index');
    Route::get('categories/fetch', 'CategoryController@index');
    Route::resource('categories', 'CategoryController')
         ->only(['store', 'update', 'destroy']);
});

// Авторизация
Auth::routes();
Route::match(['get', 'post'], '/register', function () {
    return redirect()->route('start');
})->name('register');
