<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    /**
     * Все публикации с постраничным выводом
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Получаем по 10 записей на страницу
        $posts = Post::orderBy('created_at', 'desk')
                     ->with('categories')
                     ->paginate(10);

        return view('site.posts.index', compact('posts'));


    }

    /**
     * Страница определенной публикации
     *
     * @param  \App\Post $post
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('site.posts.show',
            ['post' => $post->load('categories')]);
    }
}
