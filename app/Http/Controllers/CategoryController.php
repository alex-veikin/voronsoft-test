<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * Стриница категорий
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Получаем коллекцию категорий
        $categories = Category::withCount('posts')->get();

        return view('site.categories.index',
            compact('categories'));
    }

    /**
     * Страцица определенной категории
     * с постраничным выводом публикаций
     *
     * @param  \App\Category $category
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        // Получаем по 10 записей на страницу
        $posts = $category->posts()
                          ->orderBy('created_at', 'desk')
                          ->paginate(10);

        return view('site.categories.show', compact(['category', 'posts']));
    }
}
