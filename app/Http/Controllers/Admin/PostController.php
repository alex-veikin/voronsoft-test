<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;

class PostController extends Controller
{

    /**
     * Получаем все записи постранично
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // Получаем по 10 записей на страницу
        $posts      = Post::orderBy('created_at', 'desk')
                          ->with('categories')
                          ->paginate(20);
        // Получаем список категорий
        $categories = Category::all(['id', 'title']);

        // Ответ
        return response()->json([
            'posts'      => $posts,
            'categories' => $categories,
        ]);
    }

    /**
     * Создаем запись
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // Создаем запись
        /** @var Post $post */
        $post = Post::create([
            'title'   => $request->input('title'),
            'content' => $request->input('content'),
        ]);

        // Привязываем категории
        $post->categories()->sync($request->input('categories'));

        // Ответ
        return response()->json(['post' => $post->load('categories')]);
    }

    /**
     * Обновляем запись
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        // Обновляем данные
        Post::find($id)->update([
            'title'   => $request->input('title'),
            'content' => $request->input('content'),
        ]);

        /** @var Post $post */
        $post = Post::find($id);

        // Привязываем категории
        $post->categories()->sync($request->input('categories'));

        // Ответ
        return response()->json(['post' => $post->load('categories')]);
    }

    /**
     * Удаляем запись
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        // Получаем запись
        $post = Post::find($id);

        // Ответ
        return response()->json(['success' => $post->delete()]);
    }
}
