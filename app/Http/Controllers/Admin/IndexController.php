<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    /**
     * Admin panel
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts      = Post::count();
        $categories = Category::count();

        return view('admin.index', compact(['posts', 'categories']));
    }

    /**
     * Вывод шаблона публикаций
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function posts()
    {
        return view('admin.posts');
    }

    /**
     * Вывод шаблона категорий
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categories()
    {
        return view('admin.categories');
    }
}
