<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{

    /**
     * Получаем все категории
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // Получаем категории
        $categories = Category::all();

        // Ответ
        return response()->json(['categories' => $categories]);
    }

    /**
     * Создаем запись
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // Создаем запись
        $category = Category::create(['title' => $request->input('title')]);

        // Ответ
        return response()->json(['category' => $category]);
    }

    /**
     * Обновляем запись
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        // Обновляем данные
        Category::find($id)->update(['title' => $request->input('title')]);

        // Ответ
        return response()->json(['category' => Category::find($id)]);
    }

    /**
     * Удаляем запись и возвращаем одну в конец списка
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        // Получаем запись
        $category = Category::find($id);

        // Ответ
        return response()->json(['success' => $category->delete()]);
    }
}
