<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * Show start page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desk')
                     ->take(10)
                     ->get();

        return view('site.index', compact('posts'));
    }
}
